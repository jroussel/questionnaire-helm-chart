# Changelog

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/compare/release/1.1.0...release/1.2.0) (2024-01-16)


### Features

* update app version to 1.1.0 ([9793fad](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/9793fadb06eccdcf383ff9aee7ee5646735cf263))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/compare/release/1.0.0...release/1.1.0) (2023-10-18)


### Features

* add cronjob resource for questionnaire-cron container ([438d388](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/438d388683f59075a659e56b0da6e91efc8f3da9))

## 1.0.0 (2023-10-17)


### Features

* first stable version ([32e404c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/32e404c9cf3dd26e10c55b88c09025fdf800b38b))
* initial helm release ([ff10337](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/ff10337314fac7fa96272d78925bab928f5731a9))
* initial helm release ([fba8461](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/fba8461621097cad4a228719929eae9b51048327))
* modifications to resolve issues 3-4-5 ([c8e186c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/c8e186c1f37991067a48a9fd6a43f51c1e135dd1))
* new testing version ([55837f5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/55837f56ca1f2e3e8dbf2a2329ee42fd375c2d2b))
* publish testing version ([9e246ef](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/9e246ef2f40830ee5f7ef935b9c73ed608785b50))
* refactor helm chart ([de342fd](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/de342fd5af4920be362e2d00fecee599a1bd730c))


### Bug Fixes

* add correct .gitlab-ci.yml ([105d0fd](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/105d0fd29a9aee1a4a9a8969fd4ae62eabdf7bad))
* correct typo in values.yaml ([8ed45c0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/8ed45c0f9ff0e60ad3fbd325ff25e62647401b06))
* modified secrets so container will work ([5327aae](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/5327aae2c29ecdf89112e97cc76df5258a06fe46))
* need intel about keycloak conf in js ([d6c127f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/d6c127f2ea4fa9c3f233720e3ffa40cb6d49fafa))
* wip templating from vars.ini ([ada5c61](https://gitlab.mim-libre.fr/EOLE/eole-3/services/questionnaire/questionnaire-helm-chart/commit/ada5c61f503d7a5fad6dd85cefd936772626dee9))
